var path = require('path');

module.exports = {
  entry: './server/app.js',
  target: 'node',
  node: {
    __dirname: true
  },
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'backend.js'
  }
};
