import mongoose from 'mongoose'

const chatMessage = new Schema ({
  isSender: Boolean,
  message: String,
  sessionId: String
});

export default mongoose.model('Message',chatMessage);
