import mongoose from 'mongoose'

const chatSession = new Schema ({
  sessionId: {type: String, index: {unique: true}}
});

export default mongoose.model('Session',chatSession);
