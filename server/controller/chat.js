
function processWsRequest(req) {
  const connection = req.accept(null, req.origin);
  console.log("conntection", connection);
  connection.on('message', (msg)=> {
    console.log("message", msg);
  });
  connection.on('close', (msg)=> {
    console.log("closing connection ...");
  });
}

export default processWsRequest
