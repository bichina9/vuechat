import Session from '../model/session'
import uuidv4 from 'uuid/v4'

function newSession() {
  //generate new session and return its unique id
  const uuid = uuidv4();
  new Session(uuid).save((err) => {
    if (!err) {
      console.log("session " , uuid , " saved")
    }
  });
  return uuid
}
