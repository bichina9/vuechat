import http from 'http'
import express from 'express'
import favicon from 'serve-favicon'
import processWsRequest from './controller/chat'

import ws from 'websocket'

let path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

const app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use('/static', express.static(__dirname + '/public'));

app.get('/', (req, res, next) => {
  res.sendFile('index.html',
    { root: __dirname + '/public/'} , (e) => {next(e)});
});

app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.send('error');
});

const server = http.createServer(app);
server.listen(3000);

const wsServer = new ws.server({
  httpServer : server
});

wsServer.on('request', processWsRequest);
